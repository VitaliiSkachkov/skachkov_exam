package com.tsystems.javaschool.tasks.zones;

import java.util.*;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     * <p>
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     * - A has link to B
     * - OR B has a link to A
     * - OR both of them have a link to each other
     *
     * @param zoneState        current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds) {
        // TODO : Implement your solution here

        System.out.println("zones number:" + zoneState.size());

        if (!isZoneListCorrect(zoneState, requestedZoneIds))
            return false;

        int[][] ArrayOfZones = new int[zoneState.size()][zoneState.size()];


        for (int i = 0; i < zoneState.size(); i++) {
            for (int j = 0; j < zoneState.size(); j++) {
                if (areZonesAdjacent(i, j, zoneState)) {
                    ArrayOfZones[i][j] = 1;
                    ArrayOfZones[j][i] = 1;
                }
            }
        }
        printAdjacentArray(ArrayOfZones);


        List<Integer> requestedZoneIdsNormalized = new ArrayList<Integer>();

        for (int Node :
                requestedZoneIds) {
            requestedZoneIdsNormalized.add(Node - 1);
        }

        System.out.println("Normalized list: ");
        printZonesList(requestedZoneIdsNormalized);
        System.out.println();
        System.out.println("Original  list: ");
        printZonesList(requestedZoneIds);
        System.out.println();


        int[] requestedZoneIdsNormalArray = new int[requestedZoneIds.size()];
        requestedZoneIdsNormalArray = toIntArray(requestedZoneIdsNormalized);
        int[][] extracted = extractSubgraphFromGraph(ArrayOfZones, requestedZoneIdsNormalArray, requestedZoneIdsNormalized);

        printAdjacentArray(extracted);

        boolean result = checkGraphConnectivity(extracted);
        System.out.println(result);
        System.out.println("-------------------------------------------------------------------------------------------");
        return result;
    }

    private int[][] extractSubgraphFromGraph(int[][] graph, int[] varietyArray, List<Integer> variety) {

        int[][] subGraph = new int[varietyArray.length][varietyArray.length];
        int m = 0;
        for (int i = 0; i < graph.length; i++) {

            if (variety.contains((Object) i)) {
                int n = 0;
                for (int j = 0; j < graph.length; j++) {

                    if (variety.contains((Object) j)) {

                        if (graph[i][j] == 1) {
                            if (i != j) {
                                subGraph[m][n] = 1;
                                subGraph[n][m] = 1;
                            }

                        }
                        n++;
                    }
                }
                m++;
            }

        }

        return subGraph;
    }


    private boolean checkGraphConnectivity(int[][] extracted) {
        boolean[] visited = new boolean[extracted.length];
        Queue<Integer> Q = new LinkedList<>();
        for (int i = 0; i < extracted.length; i++) {
            visited[i] = false;
        }

        List<Integer> initial = new ArrayList<Integer>(extracted.length);

        for (int i = 0; i < extracted.length; i++) {
            initial.add(i);
        }

        Q.add(0);
        visited[0] = true;
        initial.remove((Object) 0);
        while (!Q.isEmpty()) {// пока очередь не пуста
            int currentNode = Q.poll();
            if (initial.isEmpty()) {
                return true;
            }

            for (int i = 0; i < extracted.length; i++) {
                if (extracted[currentNode][i] == 1) {// if neighbour is found
                    if (visited[i] == false) {
                        Q.add(i);
                        visited[i] = true;
                        initial.remove((Object) i);
                    }
                }
            }

        }
        return false;
    }

    private void printAdjacentArray(int[][] arrayOfZones) {

        for (int i = 0; i < arrayOfZones.length; i++) {
            for (int j = 0; j < arrayOfZones.length; j++) {
                System.out.print(arrayOfZones[i][j] + " ");
            }
            System.out.println();
        }
    }

    private boolean isZoneListCorrect(List<Zone> zoneState, List<Integer> requestedZoneIds) {
        List<Integer> zoneIDs = new ArrayList<>();
        for (Zone z : zoneState
                ) {
            zoneIDs.add(z.getId());
        }

        for (int numberToCheck :
                requestedZoneIds) {
            if (!zoneIDs.contains(numberToCheck))
                return false;
        }
        return true;
    }


    private int[] toIntArray(List<Integer> list) {
        int[] ret = new int[list.size()];
        for (int i = 0; i < ret.length; i++)
            ret[i] = list.get(i);
        return ret;
    }

    private boolean areZonesAdjacent(int zoneA, int zoneB, List<Zone> zoneState) {
        for (int k = 0; k < zoneState.size(); k++) {
            if (zoneState.get(k).getId() == (zoneA + 1))
                if (zoneState.get(k).getNeighbours().contains((int) (zoneB + 1))) {
                    return true;
                } else
                    return false;

        }
        return false;
    }

    private void printZonesList(List<Integer> Zones) {
        for (int zoneNumber : Zones
                ) {
            System.out.print(" " + zoneNumber);
        }
    }


}
