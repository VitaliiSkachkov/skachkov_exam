package com.tsystems.javaschool.tasks.pyramid;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        // TODO : Implement your solution here
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        int numberOfElements = inputNumbers.size();


        if (!isNumberTriangular(numberOfElements))
            throw new CannotBuildPyramidException();

        int rows = getTriangularRoot(numberOfElements);
        int columns = 2 * rows - 1;
        int[][] resultPyramid = new int[rows][columns];

        Collections.sort(inputNumbers);
        System.out.println("Printed and sorted list     ");
        for (int i = 0; i < inputNumbers.size(); i++) {
            System.out.print(inputNumbers.get(i) + " ");
        }
        System.out.println();


        for (int i = 0; i < inputNumbers.size(); i++) {
            int rowNumber = findRowNumberInArray(i, inputNumbers.size());
            int columnNumberOfFirst = findColumnNumberInArray(i, inputNumbers.size());
            int columnNumber = (columnNumberOfFirst + 2 * (i - findTriangularNumberLessThanCurrentNumber(i)));
            int value = inputNumbers.get(i);
            resultPyramid[rowNumber][columnNumber] = value;
        }
        System.out.println();


        return resultPyramid;
    }


    private int findRowNumberInArray(int serialNumberInList, int totalNumberOfElements) {
        int rowNumber = (int) (Math.sqrt(serialNumberInList * 8 + 1) - 1) / 2;
        return rowNumber;
    }

    private int findColumnNumberInArray(int serialNumberInList, int totalNumberOfElements) {

        int ColumnOfFirstElementInRow = 0;
        int closestTriangularNumber = findTriangularNumberLessThanCurrentNumber(serialNumberInList);
        int rowNumber = findRowNumberInArray(serialNumberInList, totalNumberOfElements);
        ColumnOfFirstElementInRow = totalRowsNumber(totalNumberOfElements) - rowNumber - 1;
        return ColumnOfFirstElementInRow;
    }

    private int findTriangularNumberLessThanCurrentNumber(int serialNumberInList) {
        int row = totalRowsNumber(serialNumberInList);
        int closestTriangular = ((row * 2 + 1) * (row * 2 + 1) - 1) / 8;
        return closestTriangular;
    }

    private int totalRowsNumber(int numberOfElements) {
        return (int) (Math.sqrt(numberOfElements * 8 + 1) - 1) / 2;
    }

    private boolean isNumberTriangular(int numberOfElements) {
        double a = (Math.sqrt(numberOfElements * 8 + 1) - 1) / 2;
        if (a > 0) {
            if ((a == Math.floor(a)) && !Double.isInfinite(a)) {
                return true;
            } else return false;
        } else return false;
    }

    private int getTriangularRoot(int numberOfElements) {
        double a = (Math.sqrt(numberOfElements * 8 + 1) - 1) / 2;
        return (int) a;
    }

}


